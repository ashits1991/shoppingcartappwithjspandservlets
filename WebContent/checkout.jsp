<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@page import="java.util.*, com.ashitsathish.mvc.model.CompleteShoppingProduct"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CheckOut</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
	div{
		text-align:center;
	}
	
	table {
    border-radius: 5px;
    width: 50%;
    margin: 0px auto;
    float: none;
    }
</style>
<body>

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="main.jsp"></jsp:include>
<div>
	<h1>Here is your final Summary</h1>
 <% Double finalPrice = (Double)session.getAttribute("finalPrice");
 	HashMap<String,Double> listOfProducts = (HashMap<String,Double>)session.getAttribute("selectedProudcts"); %>
 	<table>
			<tr>
				<th>Product Name</th>
				<th>Product Individual Price</th>
			</tr>
 	
	<% for(String s: listOfProducts.keySet()){%>
		
			
			<tr>
				<td><%= s %></td>
				<td><%=listOfProducts.get(s) %></td>
			</tr>
		
		
	<% }%>

	 </table>
	 
	 <hr>
	 <h1>The Final Price of the items is <%=finalPrice %></h1>
	 
	 <form action="cart" method="post">
	 
	 <input type="submit" class="btn btn-primary" name="action" value="Back to Cart"/>
   	 <input type="submit" class="btn btn-primary" name="action" value="Thank You"/>
     <input type='hidden' name='page' value='checkout' />	
	 </form>
	 
	  </div>
	  <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>