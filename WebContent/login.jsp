<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %> --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Page</title>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
  <style>
  	.login{
  		color: red;
  	}
  	
  </style>
<body>

<form class="form-inline" action="Controller" method="POST">

<% if(request.getAttribute("loginError") == null){%>
	<h1>Please Login</h1>
<% }%>

<% 
	if(request.getAttribute("loginError") != null){ 
	String errorMessage = (String)request.getAttribute("loginError");
%>
	<h1 class="login"><%= errorMessage %></h1>
<%}%>

  <div class="form-group">
      <label for="username">UserName:</label>
      <input type="text" class="form-control" id="username" 
      placeholder="Enter username" name="username">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" 
      placeholder="Enter password" name="password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="reset" class="btn btn-danger">Reset</button>
    <input type='hidden' name='page' value='login' />
</form>         

</body>
</html>