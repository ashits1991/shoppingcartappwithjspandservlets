package com.ashitsathish.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ashitsathish.mvc.model.ShoppingProduct;
import com.ashitsathish.mvc.model.Login;
import com.ashitsathish.mvc.model.CompleteShoppingProduct;
import com.ashitsathish.mvc.model.DbUtil;


/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public HttpSession session;
	public HttpSession session1;
	@Resource(name="jdbc/jsp_servlets")
    private DataSource dataSource;
	private DbUtil dbUtil;
	private List<CompleteShoppingProduct> selectedProducts;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		dbUtil = new DbUtil(dataSource);
	}



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String action = request.getParameter("action");
		String page = request.getParameter("page");
		String nextPage="/login.jsp";
		session = request.getSession();	
		
		if(page!= null&& page.equalsIgnoreCase("login")) {	
			String username=request.getParameter("username");
			String password=request.getParameter("password");
			boolean isAuthenticated=loginService(username, password);
			if(isAuthenticated) {
				nextPage= "/cart.jsp";
				List<ShoppingProduct> products = dbUtil.getShoppingProducts();
				session.setAttribute("products", products);
			}else {
				nextPage= "/login.jsp";
				request.setAttribute("loginError","Incorrect username or password, Please try again");
			}	
		}
		
		else if( page.equalsIgnoreCase("cart")) {
			if(action.trim().equalsIgnoreCase("Add to Cart")) {
				selectedProducts = new ArrayList();

				String[] idArr = request.getParameterValues("checkedItem");	
				for(String s: idArr){
					int id = Integer.valueOf(s);
					ShoppingProduct individualProduct = dbUtil.getIndividualProductFromDb(id);
					String quantityInString = request.getParameter(s);
					if(quantityInString != null) {
					int quantity = Integer.valueOf(quantityInString);
					CompleteShoppingProduct completeproduct = new CompleteShoppingProduct(individualProduct ,quantity);
					selectedProducts.add(completeproduct);	
					}
				}
				request.setAttribute("message", "Items added to the list");
				nextPage="/cart.jsp";
			}else if(action.trim().equalsIgnoreCase("Checkout")) {
				nextPage ="/checkout.jsp";
				session.setAttribute("selectedProudcts",peformFinalBill());
			}
		
		}
		else if( page.equalsIgnoreCase("checkout")) {
			if(action.trim().equalsIgnoreCase("Back to Cart")) {
				nextPage="/cart.jsp";
			}else if(action.trim().equalsIgnoreCase("Thank You")) {
				nextPage="/thankyou.jsp";
			}
		}
		
		else if(page.equalsIgnoreCase("main")) {
			if(action.trim().equalsIgnoreCase("help")) {
				nextPage="/help.jsp";
			}else if(action.trim().equalsIgnoreCase("logout")) {
				session.invalidate();
				nextPage="/login.jsp";
			}
		}
		
		else if(page.equalsIgnoreCase("main")) {
			if(action.trim().equalsIgnoreCase("help")) {
				nextPage="/help.jsp";
			}else if(action.trim().equalsIgnoreCase("logout")) {
				session.invalidate();
				nextPage="/login.jsp";
			}
		}
	
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
		dispatcher.forward(request, response);
		
	}
	
	private boolean loginService(String username, String password) {
		List<Login> logins = dbUtil.getLoginInfo();
		for(Login login: logins) {
			if(username.equals(login.getUsername()) && 
					password.equals(login.getPassword())) {
				return true;
			}
		}
		return false;
		
	}
	
private HashMap<String, Double> peformFinalBill() {
	double finalPrice =0;
	HashMap<String, Double> hashMap = new HashMap();
	if(selectedProducts!=null) {
	for(CompleteShoppingProduct s :selectedProducts) {
	String name  = s.getShoppingProduct().getName();
	double productsPrice = s.getQuantity()*s.getShoppingProduct().getPrice();
	finalPrice += productsPrice; 
	hashMap.put(name, productsPrice);
	}
	}
	session.setAttribute("finalPrice",finalPrice);
	return hashMap;
	}


}
