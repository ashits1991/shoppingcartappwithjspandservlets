package com.ashitsathish.mvc.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public class DbUtil {
	
	private DataSource dataSource;

	public DbUtil(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	public List<Login> getLoginInfo(){
		List<Login> logins = new ArrayList();
		String sql = "SELECT * FROM login";
		try (Connection conn = dataSource.getConnection();
				Statement stmt  = conn.createStatement();
				ResultSet  rS = stmt.executeQuery(sql)){
			
			while(rS.next()) {
				Login login = new Login();
				login.setUsername(rS.getString("username"));
				login.setPassword(rS.getString("password"));
				logins.add(login);
			}
			
			return logins;
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ShoppingProduct> getShoppingProducts(){
		List<ShoppingProduct> shoopingProducts = new ArrayList();
		
		String sql = "SELECT * FROM item";
		try (Connection conn = dataSource.getConnection();
				Statement stmt  = conn.createStatement();
				ResultSet  rS = stmt.executeQuery(sql)){
			
			while(rS.next()) {
				ShoppingProduct individualProduct = new ShoppingProduct();
				individualProduct.setId(rS.getInt("id"));
				individualProduct.setName(rS.getString("name"));
				individualProduct.setPrice(rS.getDouble("price"));
				shoopingProducts.add(individualProduct);
				
			}
			
			return shoopingProducts;
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ShoppingProduct getIndividualProductFromDb(int id) {
		String sql = "SELECT * FROM item WHERE id= ?";
		ShoppingProduct individualProduct = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement pS = conn.prepareStatement(sql) ){	
				
					pS.setInt(1, id);
					ResultSet rS = pS.executeQuery();
					while(rS.next()) {
						individualProduct = new ShoppingProduct();
						individualProduct.setId(id);
						individualProduct.setName(rS.getString("name"));
						individualProduct.setPrice(rS.getDouble("price"));
					}
					
					return individualProduct;
				}catch(SQLException e) {
					e.printStackTrace();
				}
		return null;
	}
}
